package br.com.mv.gh.desafioghdev1.integration.model;

import java.util.List;

public class StatesDTOList {
    private List<StateDTO> states;

    public StatesDTOList() {
    }

    public List<StateDTO> getStates() {
        return states;
    }

    public void setStates(List<StateDTO> states) {
        this.states = states;
    }
}
