package br.com.mv.gh.desafioghdev1.integration.service;

import br.com.mv.gh.desafioghdev1.integration.model.StateDTO;

import java.util.List;

public interface StateServiceClient {
    List<StateDTO> listStates(String lang);
}
