package br.com.mv.gh.desafioghdev1.integration.service.impl;

import br.com.mv.gh.desafioghdev1.integration.model.StateDTO;
import br.com.mv.gh.desafioghdev1.integration.model.StatesDTOList;
import br.com.mv.gh.desafioghdev1.integration.service.StateServiceClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class StateServiceClientBean implements StateServiceClient {
    private final RestTemplate restTemplate;
    private final Logger logger;

    @Value("${state.api.url}")
    protected String apiUrl;

    @Autowired
    public StateServiceClientBean(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
        this.logger = LoggerFactory.getLogger(getClass());
    }

    @Override
    public List<StateDTO> listStates(String lang) {
        logger.info("Invocando o serviço externo para consulta de estados para a lang [{}]", lang);
        ResponseEntity<StatesDTOList> responseEntity = restTemplate.getForEntity(apiUrl, StatesDTOList.class);
        if (responseEntity.getStatusCode().is2xxSuccessful()) {
            List<StateDTO> states = responseEntity.getBody().getStates();
            logger.info("O serviço retornou o total de {} estados", states.size());
//            List<StateDTO> filteredStates = states.stream().filter(state -> "en".equals(state.getLang())).collect(Collectors.toList());
            List<StateDTO> filteredStates = states.stream().filter(state -> lang.equals(state.getLang())).collect(Collectors.toList());
            logger.info("Total de estados após o filtro foi de: {}", filteredStates.size());
            return filteredStates;
        } else {
            throw new HttpClientErrorException(responseEntity.getStatusCode());
        }
    }
}
