package br.com.mv.gh.desafioghdev1.rest;

import br.com.mv.gh.desafioghdev1.model.State;
import br.com.mv.gh.desafioghdev1.service.StateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "api/state")
public class StateResource {
    @Autowired
    private StateService stateService;

    @PostMapping("/update/{lang}")
    public ResponseEntity<Void> updateStates(@PathVariable String lang) {
        stateService.updateStates(lang);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public List<State> list() {
        return stateService.list();
    }
}
