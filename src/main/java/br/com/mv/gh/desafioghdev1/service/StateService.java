package br.com.mv.gh.desafioghdev1.service;

import br.com.mv.gh.desafioghdev1.integration.model.StateDTO;
import br.com.mv.gh.desafioghdev1.integration.service.StateServiceClient;
import br.com.mv.gh.desafioghdev1.model.State;
import br.com.mv.gh.desafioghdev1.repository.StateRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class StateService {

    private final StateServiceClient stateServiceClient;
    private final StateRepository stateRepository;
    private final Logger logger;

    @Autowired
    public StateService(StateServiceClient stateServiceClient, StateRepository stateRepository) {
        this.stateServiceClient = stateServiceClient;
        this.stateRepository = stateRepository;
        logger = LoggerFactory.getLogger(getClass());
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void updateStates(String lang) {
        List<StateDTO> stateDTOS = stateServiceClient.listStates(lang);
        List<State> statesEntity = stateDTOS.stream().map(dto -> new State(dto.getTitle(), dto.getUrl())).collect(Collectors.toList());
        logger.info("Excluindo todos os estados da base de dados local");
        stateRepository.deleteAll();
        logger.info("Salvando {} estados na base de dados local",statesEntity.size());
        stateRepository.saveAll(statesEntity);
    }

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<State> list() {
        return stateRepository.findAll(Sort.by(Sort.Direction.ASC,"title"));
    }
}
