package br.com.mv.gh.desafioghdev1;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.junit.jupiter.api.*;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.containsString;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class DesafioGhDev1ApplicationTests {

    @LocalServerPort
    private int port;

    @BeforeEach
    public void setUp() {
        RestAssured.port = port;
    }

    @Test
    @Order(1)
    public void testUpdateStates() {
        given()
                .when()
                .post("/api/state/update/en")
                .then()
                .statusCode(201);
    }

    @Test
    @Order(2)
    public void testListStates() {

                given()
                .when()
                .get("/api/state/list")
                .then()
                .statusCode(200)
//                .body("lang", everyItem(equalTo("en")));
                        .body(containsString("Washington"))
                        .body("$.size()", equalTo(56));
    }
}
